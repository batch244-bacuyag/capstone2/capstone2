const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController")
const auth = require("../auth")

// Route for creating a product
router.post("/", auth.verify, (req, res) => {
	try {
		if (auth.decode(req.headers.authorization).isAdmin)
			productController.addProduct({product: req.body, isAdmin: true}).then(result => res.send(result));
		else res.send(true);
	} catch (error) {
		res.status(500).send(`Error: ${error.message}`);
	}
});



//Retrieving all Active products
router.get("/", (req, res) =>{
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});

//Retrieving all products
router.get("/all", (req, res) =>{
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

//Retrieving a specific product
router.get("/:productId", (req,res) =>{
    console.log("hello")
    console.log(req.params.productId)
    productController.getProduct(req.params).then(resultFromController => {
        console.log("Result from controller:", resultFromController);
        res.send(resultFromController);
    });
});

//Updating a product
router.put("/:productId", auth.verify, async (req, res) => {
    const data = auth.decode(req.headers.authorization);
    if(data.isAdmin) {
        try {
            const resultFromController = await productController.updateProduct(req.params, req.body);
            res.send(resultFromController);
        } catch (error) {
            console.log(error);
            res.status(500).send(`Error: ${error.message}`);
        }
    } else {
        res.send("Access denied!");
    }
});

// Route to archive a product

router.patch("/:productId/archive", auth.verify, async (req, res) =>{
	try {
		const data = auth.decode(req.headers.authorization);
		if(data.isAdmin){
			const result = await productController.archiveProduct(req.params, req.body);
			res.send(result);
		} else{
			res.send("Access denied!");
		}
	} catch (error) {
		console.log(error);
		res.status(500).send(`Error: ${error.message}`);
	}
});

/*router.get("/:productName", (req,res) =>{
  console.log(req.params.productName);
  productController.getProduct(req.params.productName)
    .then(resultFromController => res.send(resultFromController))
    .catch(error => {
      console.error('Error:', error);
      res.status(404).send(error.message);
    });
});*/


module.exports = router;