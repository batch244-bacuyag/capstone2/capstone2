const express = require("express");
const router = express.Router();
const userController =require("../controllers/userController");
const auth = require("../auth");

//user registration
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send (resultFromController))	
});

//Route for the user authentication
router.post("/login", (req, res) =>{
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

//add to cart 
router.put("/cart", auth.verify, (req, res) => {

	let data = {
		userId : auth.decode(req.headers.authorization).id,

		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		productId : req.body.productId,
		quantity : req.body.quantity
	}
	console.log(data)

if (!data.isAdmin && data.quantity === 0){
    userController.removeProductFromOrder(data).then(resultFromController => res.send(resultFromController));
}

if (!data.isAdmin && data.quantity !== 0) {
	console.log("hello")
	console.log(data)
	console.log("hello")
    userController.addToCart(data).then(resultFromController => res.send(resultFromController));

} /*else {
    res.status(401).json({ message: "Access denied" });
}*/

});

//checkout
router.put("/checkout/", auth.verify, (req, res) => {

	let data = {
		userId : auth.decode(req.headers.authorization).id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data)
	if (!data.isAdmin){
		userController.checkout(data).then(resultFromController => res.send(resultFromController));
	}else{
		res.send("Access denied")		
	}
})

//retrieve user orders
router.get("/myOrders", auth.verify, (req, res) => {

    //Uses the "decode" method defined in the auth.js to retrieve the user information from the token, passing the "token" from the request header as an arguement
    const userData = auth.decode(req.headers.authorization);
    console.log(userData);
    // Provides the user's ID for the getProfile controller method
    userController.checkMyOrders({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});


router.get("/details", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    console.log(userData);

    userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
})

router.patch("/:userId/setAsAdmin", auth.verify, async (req, res) =>{
	try {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
		if(data.isAdmin){
			const result = await userController.setAsAdmin(req.params, req.body);
			res.send(result);
		} else{
			res.send("Access denied!");
		}
	} catch (error) {
		console.log(error);
		res.status(500).send(`Error: ${error.message}`);
	}
});

router.get("/orders", (req, res) => {
  	
	let data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data)
	if (data.isAdmin){
		userController.getAllOrders().then(resultFromController => res.send(resultFromController));
	}else{
		res.send("Access denied")		
	}
})



router.get("/", (req, res) => {
  	
	let data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data)
	if (data.isAdmin){
		userController.getAllUsers().then(resultFromController => res.send(resultFromController));
	}else{
		res.send("Access denied")		
	}
})




router.patch("/:userId", auth.verify, async (req, res) =>{
	try {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
		if(data.isAdmin){
			console.log(data)
			const result = await userController.updateUsers(req.params, req.body);
			res.send(result);
		} else{
			res.send("Access denied!");
		}
	} catch (error) {
		console.log(error);
		res.status(500).send(`Error: ${error.message}`);
	}
});

router.get("/myOrders", auth.verify, (req, res) => {

    //Uses the "decode" method defined in the auth.js to retrieve the user information from the token, passing the "token" from the request header as an arguement
    const userData = auth.decode(req.headers.authorization);
    console.log(userData);
    // Provides the user's ID for the getProfile controller method
    userController.checkMyOrders({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});




module.exports = router;

