const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");

//MongoDB connection
mongoose.connect("mongodb+srv://admin:admin@zuitt-course-booking.cwrxp6r.mongodb.net/Capstone_Ecommerce?retryWrites=true&w=majority", 
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once(`open`, () => 
	console.log(`Now connected to the MongoDB atlas.`));

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

app.use("/users", userRoutes);
app.use("/products", productRoutes);

app.listen(process.env.PORT || 4001, () =>{
	console.log(`API is now online at port ${process.env.PORT || 4001}`)
});