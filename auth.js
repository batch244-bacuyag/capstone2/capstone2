const jwt = require("jsonwebtoken");

const secret = "ECommerceAPI"

module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {});
}

//Token Verification
module.exports.verify = (req, res, next) => {
    const token = req.headers.authorization;
    if (!token) return res.send({ auth: "failed" });

    try {
        jwt.verify(token.slice(7), secret);
        next();
    } catch (err) {
        res.send({ auth: "failed" });
    }
}

//Token decryption
module.exports.decode = (token) => {
    if (!token) return null;
    try {
        return jwt.verify(token.slice(7), secret);
    } catch (err) {
        return null;
    }
}