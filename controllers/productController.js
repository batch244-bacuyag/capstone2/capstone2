const Product = require("../models/Product");


// Create a new prduct
module.exports.addProduct = async data => {
  try {
    console.log(data)
    if(await Product.findOne({name: data.product.name})) return true;
    await new Product(data.product).save();
    return false;
} catch (error) {
    console.log(error);
    return {error: "Something went wrong"};
}
};


//Retrieve all active products
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result =>{
		return result;
	})
};

//Retrieve all active products
module.exports.getAllProducts = () => {
    return Product.find().then(result =>{
        return result;
    })
};


//Retrieving a product

module.exports.getProduct = (reqParams) => {
  console.log("productId:", reqParams.productId);
  
  return Product.findById(reqParams.productId).then(result => {
    console.log("result:", result);
    return result;
  });
}

//updating a product
module.exports.updateProduct = async (reqParams, reqBody) => {
    try {

        const existingProduct = await Product.findById(reqParams.productId);

        let updatedProduct = {
            name: reqBody.name,
            description: reqBody.description,
            price: reqBody.price,
            isActive: reqBody.isActive,
            imageUrl: reqBody.imageUrl
        };
        await Product.findByIdAndUpdate(reqParams.productId, updatedProduct);
        return ;
    } catch (error) {
        console.log(error);
        return {error: "Something went wrong"};
    }
};

//Archiving a product
module.exports.archiveProduct = async (reqParams, reqBody) => {
    try {

        const existingProduct = await Product.findById(reqParams.productId);
        if(existingProduct.isActive === reqBody.isActive) return "Status has not been updated. Please enter new status!";

        let updateActiveField = {
            isActive : reqBody.isActive
        };

        await Product.findByIdAndUpdate(reqParams.productId, updateActiveField);

        if(!reqBody.isActive)
        return "Product archived";

        if(reqBody.isActive)
        return "Product unarchived";

    } catch (error) {
        console.log(error);
        return {error: "Something went wrong"};
    }
};







