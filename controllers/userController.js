const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User");
const Product = require("../models/Product");
const { isEmail } = require('validator');



module.exports.registerUser = async (reqbody) => {
    try {
        if(!isEmail(reqbody.email)) return "Invalid email format.";
        const existingUser = await User.findOne({email: reqbody.email});
        if(existingUser){
            console.log("Email already registered!")
            return true;
        } 

        const newUser = new User({
            firstName: reqbody.firstName,
            lastName: reqbody.lastName,
            email: reqbody.email,
            mobileNo: reqbody.mobileNo,
            password: bcrypt.hashSync(reqbody.password, 10)
        });
        await newUser.save();
        console.log("You've successfully registered your email!")
        return false;
    } catch (error) {
        console.log(error);
        return {error: "Something went wrong"};
    }
}


module.exports.loginUser = async (reqbody) => {
    try {
        const result = await User.findOne({email: reqbody.email});
        if(!result){
            return true;
        }
        
        const isPasswordCorrect = bcrypt.compareSync(reqbody.password, result.password);
        if(isPasswordCorrect) {
            return {access: auth.createAccessToken(result)};
        } else {
            return false;
        }
        
    } catch (error) {
        console.log(error);
        return {error: "Something went wrong"};
    }
}

module.exports.addToCart = async (data) => {
    try {
        const product= await Product.findById(data.productId);
        const totalAmount = product.price * data.quantity;
        const user = await User.findById(data.userId);

        if(!product.isActive){
            return "Product no longer available!"
        }
        if(product && user){//add product correct code
            const newProduct = {
                productName : product.name,
                productId: product.id,
                quantity : data.quantity,
                imageUrl: product.imageUrl
            }
            let lastOrder = user.orders.find(order => !order.purchaseOn);
            if (!lastOrder) {
                lastOrder = {
                    products: [newProduct],
                    totalAmount: totalAmount,
                    purchaseOn: ""
                }
                user.orders.push(lastOrder);
            } else {
                const index = lastOrder.products.findIndex(p => p.productName === product.name);
                const order = user.orders.find(order => !order.purchaseOn);
                let totalAmount = 0;
                const name = order.products[index]
                if (index !== -1) {
                    lastOrder.products[index].quantity = data.quantity
                    for (let i = 0; i < order.products.length; i++) {
                        const eachProduct = order.products[i];
                        const item = await Product.findOne({ name: eachProduct.productName, price: { $exists: true } });
                        console.log(eachProduct)
                        console.log(eachProduct.quantity)
                        console.log(item.price)
                        totalAmount += eachProduct.quantity * item.price;
                    }

                    lastOrder.totalAmount = totalAmount;
                    console.log(lastOrder.totalAmount)
                    user.save()
                    console.log(`${name.productName} quantity has been updated to ${data.quantity}`)
                    console.log(user)
                    return { success: false, message: "Product quantity updated" };;
                } else {//add additional product correct code
                    lastOrder.products.push(newProduct);
                    const updatedOrder = user.orders.find(order => !order.purchaseOn);
                    for (let i = 0; i < updatedOrder.products.length; i++) {
                        const eachProduct = updatedOrder.products[i]

                        const item = await Product.findOne({ name: eachProduct.productName, price: { $exists: true } });

                        totalAmount += eachProduct.quantity * item.price;
                        console.log(totalAmount)
                    }
                    lastOrder.totalAmount = totalAmount;  
                    user.save()
                    console.log(`${product.name} with quantity of ${data.quantity} has been added to cart`)
                    return { success: true, message: "Product added to cart" };
                }
            }
            user.save()
            console.log(lastOrder)
            console.log(`${product.name} with quantity of ${data.quantity} has been added to cart`)
            return { success: true, message: "Product added to cart" }; 

        }
    } catch (err) {
        throw err;
    }
}




module.exports.removeProductFromOrder = async (data) => {
    try {
        const product= await Product.findById(data.productId);
        const user = await User.findById(data.userId);
        const order = await User.find({ "orders": { $elemMatch: { "purchaseOn": null } } });
        let lastOrder;

        if (order.length>0) {
            let lastOrder = user.orders[user.orders.length - 1];
            const index = lastOrder.products.findIndex(p => p.productName === product.name);
            let totalAmount = 0;
            if (index !== -1) {
                if (lastOrder.products.length === 1) {
                    user.orders.splice(user.orders.length - 1,1);
                    if(user.orders.length === 0) delete user.orders;
                    user.save();
                    return { success: true, message: "Product has been removed and your cart is now empty." };
                }    
                lastOrder.products.splice(index, 1);

                const updatedOrder = user.orders.find(order => !order.purchaseOn);
                for (let i = 0; i < updatedOrder.products.length; i++) {
                    const eachProduct = updatedOrder.products[i]

                    const item = await Product.findOne({ name: eachProduct.productName, price: { $exists: true } });

                    totalAmount += eachProduct.quantity * item.price;
                    console.log(totalAmount)
                }
                lastOrder.totalAmount = totalAmount 
                user.save()
                return { success: true, message: "Product removed from cart" };
            } else {
                return { success: false, message: "Item is not on cart" };
            }
        } else{
            return { success: false, message: "Unable to remove product. Your cart is empty." };
        }
    } catch (err) {
        throw err;
    }
}



module.exports.checkout = async (data) => {
    try {
        const user = await User.findById(data.userId);
        const order = user.orders.find(order => !order.purchaseOn);

        if (order) {
            let checkoutMessage = "Thank you for your purchase! Your order includes:\n";
            let totalAmount = 0;

            for (let i = 0; i < order.products.length; i++) {
                const eachProduct = order.products[i];

                const product = await Product.findOne({ name: eachProduct.productName, price: { $exists: true } });

                checkoutMessage += `- ${eachProduct.productName} x ${eachProduct.quantity} at $${eachProduct.quantity * product.price} each\n`;
                totalAmount += eachProduct.quantity * product.price;
                await Product.findOneAndUpdate({name: eachProduct.productName}, {$push: { orders: { orderId: order._id, orderedOn: new Date() } } }, { new: true });
            } 
            const update = { $set: { "orders.$.purchaseOn": new Date() } };
            const userUpdate = await User.findOneAndUpdate({ _id: data.userId, "orders.purchaseOn": null }, update, { new: true });

            checkoutMessage += `Total amount: $${totalAmount}`;
            console.log(checkoutMessage)
            return(checkoutMessage);
        }
        return (`Please add product on your cart before proceeding to checkout!`)
    } catch (error) {
        console.log(error);
        return {error: "Something went wrong"};
    }
}


module.exports.checkMyOrders = (reqBody) => {
    return Promise.all([User.findById(reqBody.userId, { orders: 1, _id: 0 }), Product.find()]).then(([result, products]) => {
        let orders = result.orders;
        let orderDetails = [];

        orders.forEach(order => {
            let orderDetail = {
                orderId: order._id,
                products: [],
                totalAmount: 0
            }
            
            let status;
            if (order.purchaseOn !== null) {
                status = "Order completed";
                order.products.forEach(product => {
                    let item = products.find(item => item.name === product.productName);
                    let productDetail = {
                        productName: product.productName,
                        productId: product.productId,
                        imageUrl: product.imageUrl,
                        quantity: product.quantity,
                        pricePerItem: product.price,
                        totalPrice: product.price * product.quantity
                    }
                    console.log(product.price)
                    orderDetail.products.push(productDetail);
                    orderDetail.totalAmount += productDetail.totalPrice;
                })
            } else {
                status = "Order has not yet been completed";
                order.products.forEach(product => {
                    let item = products.find(item => item.name === product.productName);
                    let productDetail = {
                        productName: product.productName,
                        productId: product.productId,
                        imageUrl: product.imageUrl,
                        quantity: product.quantity,
                        pricePerItem: item.price,
                        totalPrice: item.price * product.quantity
                    }
                    orderDetail.products.push(productDetail);
                    orderDetail.totalAmount += productDetail.totalPrice;
                })                
            }

            orderDetails.push({
                status,
                orderDetail
            })
        })
        console.log(orderDetails)
        return {
            data: orderDetails
        }
    });
}

module.exports.getProfile = (reqBody) => {

    return User.findById(reqBody.userId).then(result => {
        result.password = "";
        return result;
    });
};

module.exports.setAsAdmin = async (reqParams, reqBody) => {
    try {

        const existingUser = await User.findById(reqParams.userId);
 /*       if(!existingUser) return {error: "User not found"};
        if(existingUser.isAdmin === reqBody.isAdmin) return "Status has not been updated. Please enter new status!";
*/
        let updateAdminField = {
            isAdmin : reqBody.isAdmin
        };

        await User.findByIdAndUpdate(reqParams.userId, updateAdminField);
        console.log(result)
/*        if(!reqBody.isAdmin)
            return "User set as non-admin";

        if(reqBody.isAdmin)
            return "User set as admin";*/
        return result;

    } catch (error) {
        console.log(error);
        return {error: "Something went wrong"};
    }
};

module.exports.getAllOrders = async (req, res) => {
    try {
        const users = await User.find({});
        const orders = users.map(user => user.orders);
        return orders;
    } catch (error) {
        console.log(error);
        return ({ error: error.message });
    }
};

module.exports.getAllUsers = () => {
  return User.find().select('+password'); // include the password field in the response
};

module.exports.updateUsers = async (reqParams, reqBody) => {
    try {
        const existingUser = await User.findById(reqParams.userId);
        let updateAdminField = {
            firstName : reqBody.firstName,
            lastName : reqBody.lastName,
            mobileNo: reqBody.mobileNo,
            email : reqBody.email,
            isAdmin : reqBody.isAdmin
        };

        await User.findByIdAndUpdate(reqParams.userId, updateAdminField);
        console.log(updateAdminField)

        return "User information updated successfully!";
    } catch (error) {
        console.log(error);
        return {error: "Something went wrong"};
    }
};





