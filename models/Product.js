const mongoose = require("mongoose");

const productSchema = new mongoose.Schema(
	{
		name: {
			type: String,
			required: [true, "Product name is required."]
		},
		brand: {
			type: String,
			required: [true, "Brand name is required."]
		},
		description: {
			type: String,
			required: [true, "Description is required."]
		},
		price: {
			type: Number,
			required: [true, "Price is required."]
		},
	    imageUrl: {
	      	type: String,
	      	required: [true, "Image sources are required."],
/*	      validate: {
	        validator: function(v) {
	          return v && v.length > 0 && v.every(src => typeof src === 'string');
	        },
	        message: props => `${props.value} is not a valid image source.`
	      }*/
	    },
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: new Date()
		},
		orders: [
			{
				orderId: {
					type: String,
					required: [true, "order ID is required."]
				},
				orderedOn: {
					type: Date,
					default: new Date()
				}			
			}
		]	
	}
)

module.exports = mongoose.model("Product", productSchema)
