const mongoose = require("mongoose");
const { isEmail } = require('validator');


const userSchema = new mongoose.Schema(
	{
		firstName: {
			type: String,
			required: [true, "First name is required."],
		},
		lastName: {
			type: String,
			required: [true, "Last name is required."],
		},
		mobileNo: {
			type: Number,
			required: [true, "Mobile number is required."],	
		},			
		email: {
			type: String,
			required: [true, "Email is required."],
			validate: [isEmail, 'Invalid email format']
		},		
		password: {
			type: String,
			required: [true, "Password is required."],		
		},
		isAdmin: {
			type: Boolean,
			default: false
		},
		orders: [
		{
			products: [{
				productName: { 
					type: String, 
					required: [true, "Product name is required."]
				},
				productId: { 
					type: String, 
					required: [true, "Product ID is required."]
				},
				quantity: {
					type: Number,
					required: [true, "Quantity is required."]
				},
			    imageUrl: {
			      	type: String,
			      	required: [true, "Image sources are required."],
		/*	      validate: {
			        validator: function(v) {
			          return v && v.length > 0 && v.every(src => typeof src === 'string');
			        },
			        message: props => `${props.value} is not a valid image source.`
			      }*/
			    }				
			}],
			totalAmount: { 
				type: Number, 
				required: [true, "Total amount is required."]
			},
			purchaseOn: { 
				type: Date, 
				default: new Date()
			}
		}
		]
	}
)

module.exports = mongoose.model("User", userSchema)